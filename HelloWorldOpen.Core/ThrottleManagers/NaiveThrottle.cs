﻿using HelloWorldOpen.Core.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HelloWorldOpen.Core.ThrottleManagers
{
    public static class NaiveThrottle
    {
        public static double Compute(CarPositions carPositions)
        {
            if (carPositions.data[0].angle == 0)
                return 1;
            else
                return .65;
        }
    }
}