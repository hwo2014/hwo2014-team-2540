﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HelloWorldOpen.Core.Track
{
    public class TrackPiece
    {
        public double Length { get; private set; }

        public bool Switch { get; private set; }

        public TrackPieceType Type { get; private set; }

        public double Angle { get; private set; }

        public double Radius { get; private set; }

        //Locked Initialization, Created only throught static methods
        private TrackPiece()
        {
        }

        //Create Factory Later to encapsulate this shit
        public static TrackPiece CreateStraightPiece(double length, bool isSwitch = false)
        {
            return new TrackPiece() { Length = length, Switch = isSwitch, Type = TrackPieceType.Straight };
        }

        public static TrackPiece CreateBendPiece(double angle, double radius)
        {
            double length = Math.Abs(angle * radius);
            return new TrackPiece() { Angle = angle, Radius = radius, Type = TrackPieceType.Bend, Length = length };
        }
    }
}