﻿using HelloWorldOpen.Core.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HelloWorldOpen.Core.Track
{
    public class RaceTrack
    {
        //Init track and hide it later
        //public for tests
        public LinkedList<TrackPiece> _raceTrack = new LinkedList<TrackPiece>();

        //Encapsulate to a Service
        public double CalculateDistance(Position startPosition, Position endPosition)
        {
            if (!_raceTrack.Contains(startPosition.TrackPiece))
                throw new ArgumentException("startPosition Track Piece is not a part of track");

            if (!_raceTrack.Contains(endPosition.TrackPiece))
                throw new ArgumentException("endPosition Track Piece is not a part of track");

            if (startPosition.TrackPiece == endPosition.TrackPiece && startPosition.PieceDistance <= endPosition.PieceDistance)
                return endPosition.PieceDistance - startPosition.PieceDistance;

            double distance = startPosition.TrackPiece.Length - startPosition.PieceDistance;
            distance += endPosition.PieceDistance;

            TrackPiece endTrackPiece = endPosition.TrackPiece;
            var currentPiece = _raceTrack.Find(startPosition.TrackPiece).NextCircular();

            while (currentPiece.Value != endTrackPiece)
            {
                distance += currentPiece.Value.Length;
                currentPiece = currentPiece.NextCircular();
            }

            return distance;
        }
    }
}