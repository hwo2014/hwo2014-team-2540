﻿using HelloWorldOpen.Core.Track;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HelloWorldOpen.Core.Track
{
    public class Position
    {
        public TrackPiece TrackPiece { get; set; }

        public double PieceDistance { get; set; }
    }
}