﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HelloWorldOpen.Core.Car
{
    public class Car
    {
        public double Velocity { get; private set; }

        public double Acceleration { get; private set; }

        public double Angle { get; private set; }

        public double AngleVelocity { get; private set; }

        public double AngleAcceleration { get; private set; }

        public double Throttle { get; set; }

        public double EngineForce { get; set; }
    }
}