﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HelloWorldOpen.Core.Json
{
    public class CarPositions
    {
        public string msgType { get; set; }

        public Datum[] data { get; set; }

        public string gameId { get; set; }

        public int gameTick { get; set; }
    }

    public class Datum
    {
        public Id id { get; set; }

        public float angle { get; set; }

        public Pieceposition piecePosition { get; set; }
    }

    public class Id
    {
        public string name { get; set; }

        public string color { get; set; }
    }

    public class Pieceposition
    {
        public int pieceIndex { get; set; }

        public float inPieceDistance { get; set; }

        public Lane lane { get; set; }

        public int lap { get; set; }
    }

    public class Lane
    {
        public int startLaneIndex { get; set; }

        public int endLaneIndex { get; set; }
    }
}