using HelloWorldOpen.Core.Json;
using HelloWorldOpen.Core.ThrottleManagers;
using Newtonsoft.Json;
using System;
using System.IO;
using System.Net.Sockets;

namespace HelloWorldOpen.AI
{
    public class Bot
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("Test Output");
            string host = args[0];
            int port = int.Parse(args[1]);
            string botName = args[2];
            string botKey = args[3];

            SendMsg joinMessage = null;
            if (args.Length > 4)
            {
                joinMessage = new JoinRace(botName, botKey, args[4]);
            }
            else
            {
                joinMessage = new Join(botName, botKey);
            }

            Console.WriteLine("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

            using (TcpClient client = new TcpClient(host, port))
            {
                NetworkStream stream = client.GetStream();
                StreamReader reader = new StreamReader(stream);
                StreamWriter writer = new StreamWriter(stream);
                writer.AutoFlush = true;

                new Bot(reader, writer, joinMessage);
            }
        }

        private StreamWriter writer;

        private Bot(StreamReader reader, StreamWriter writer, SendMsg join)
        {
            this.writer = writer;
            string line;

            send(join);

            while ((line = reader.ReadLine()) != null)
            {
                MsgWrapper msg = JsonConvert.DeserializeObject<MsgWrapper>(line);
                switch (msg.msgType)
                {
                    case "carPositions":
                        var carPositions = JsonConvert.DeserializeObject<CarPositions>(line);
                        double throttleVal = NaiveThrottle.Compute(carPositions);

                        send(new Throttle(throttleVal));
                        break;

                    case "join":
                        Console.WriteLine("Joined");
                        send(new Ping());
                        break;

                    case "gameInit":
                        Console.WriteLine("Race init");
                        send(new Ping());
                        break;

                    case "gameEnd":
                        Console.WriteLine("Race ended");
                        send(new Ping());
                        break;

                    case "gameStart":
                        Console.WriteLine("Race starts");
                        send(new Ping());
                        break;

                    default:
                        send(new Ping());
                        break;
                }
            }
        }

        private void send(SendMsg msg)
        {
            writer.WriteLine(msg.ToJson());
        }
    }

    internal class MsgWrapper
    {
        public string msgType;
        public Object data;

        public MsgWrapper(string msgType, Object data)
        {
            this.msgType = msgType;
            this.data = data;
        }
    }

    internal abstract class SendMsg
    {
        public string ToJson()
        {
            return JsonConvert.SerializeObject(new MsgWrapper(this.MsgType(), this.MsgData()));
        }

        protected virtual Object MsgData()
        {
            return this;
        }

        protected abstract string MsgType();
    }

    internal class JoinRace : SendMsg
    {
        public BotID botId;
        public string color;
        public string trackName;
        public int carCount = 1;

        public JoinRace(string name, string key, string trackName)
        {
            this.botId = new BotID()
            {
                name = name,
                key = key
            };
            this.color = "black";
            this.trackName = trackName;
        }

        protected override string MsgType()
        {
            return "joinRace";
        }
    }

    internal class BotID
    {
        public string name;
        public string key;
    }

    internal class Join : SendMsg
    {
        public string name;
        public string key;

        public Join(string botName, string botKey)
        {
            name = botName;
            key = botKey;
        }

        protected override string MsgType()
        {
            return "join";
        }
    }

    internal class Ping : SendMsg
    {
        protected override string MsgType()
        {
            return "ping";
        }
    }

    internal class Throttle : SendMsg
    {
        public double value;

        public Throttle(double value)
        {
            this.value = value;
        }

        protected override Object MsgData()
        {
            return this.value;
        }

        protected override string MsgType()
        {
            return "throttle";
        }

        public static Throttle MagicThrottleValue()
        {
            return new Throttle(0.65);
        }
    }
}