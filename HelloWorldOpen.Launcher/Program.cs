﻿using HelloWorldOpen.AI;
using System;
using System.Collections.Generic;
using System.Text;

namespace HelloWorldOpen.Launcher
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            string host = "testserver.helloworldopen.com";
            string port = "8091";
            string carName = "DinosaurMayhem";
            string carKey = "zRBC8gse5OqPKA";
            string trackName = "keimola";

            string[] startupArgs = new string[]
            {
                host,
                port,
                carName,
                carKey
                //trackName
            };

            HelloWorldOpen.AI.Bot.Main(startupArgs);
            Console.ReadKey();
        }
    }
}