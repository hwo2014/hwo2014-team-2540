﻿using FakeItEasy;
using HelloWorldOpen.Core.Track;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace HelloWorldOpen.Core.Tests.TrackTests
{
    public class RaceTrackTests
    {
        [Fact]
        public void CalculateDistance_OnTheSameTrackPiece_CorrectResult()
        {
            var trackPiece = TrackPiece.CreateStraightPiece(100);
            var startPosition = new Position() { TrackPiece = trackPiece, PieceDistance = 10 };
            var endPosition = new Position() { TrackPiece = trackPiece, PieceDistance = 90 };
            RaceTrack track = new RaceTrack();
            track._raceTrack.AddFirst(trackPiece);

            Assert.Equal(80, track.CalculateDistance(startPosition, endPosition));
        }

        [Fact]
        public void CalculateDistance_TwoAdjacentPieces_CorrectResult()
        {
            var startTrackPiece = TrackPiece.CreateStraightPiece(100);
            var finishTrackPiece = TrackPiece.CreateStraightPiece(200);
            var startPosition = new Position() { TrackPiece = startTrackPiece, PieceDistance = 20 };
            var finishPosition = new Position() { TrackPiece = finishTrackPiece, PieceDistance = 70 };

            RaceTrack track = new RaceTrack();
            var node = track._raceTrack.AddFirst(startTrackPiece);
            track._raceTrack.AddAfter(node, finishTrackPiece);

            Assert.Equal(150, track.CalculateDistance(startPosition, finishPosition));
        }

        [Fact]
        public void CalculateDistance_PositionWithoutCrossingStart_CorrectResult()
        {
            var startTrackPiece = TrackPiece.CreateStraightPiece(100);
            var middleTrackPiece = TrackPiece.CreateStraightPiece(100);
            var finishTrackPiece = TrackPiece.CreateStraightPiece(200);
            var startPosition = new Position() { TrackPiece = startTrackPiece, PieceDistance = 20 };
            var finishPosition = new Position() { TrackPiece = finishTrackPiece, PieceDistance = 70 };

            RaceTrack track = new RaceTrack();
            var node = track._raceTrack.AddFirst(startTrackPiece);
            node = track._raceTrack.AddAfter(node, middleTrackPiece);
            track._raceTrack.AddAfter(node, finishTrackPiece);

            Assert.Equal(250, track.CalculateDistance(startPosition, finishPosition));
        }

        [Fact]
        public void CalculateDistance_PositionWithCrossingStart_CorrectResult()
        {
            var startTrackPiece = TrackPiece.CreateStraightPiece(100);
            var middleTrackPiece = TrackPiece.CreateStraightPiece(100);
            var finishTrackPiece = TrackPiece.CreateStraightPiece(200);
            var startPosition = new Position() { TrackPiece = middleTrackPiece, PieceDistance = 20 };
            var finishPosition = new Position() { TrackPiece = startTrackPiece, PieceDistance = 70 };

            RaceTrack track = new RaceTrack();
            var node = track._raceTrack.AddFirst(startTrackPiece);
            node = track._raceTrack.AddAfter(node, middleTrackPiece);
            track._raceTrack.AddAfter(node, finishTrackPiece);

            Assert.Equal(350, track.CalculateDistance(startPosition, finishPosition));
        }

        [Fact]
        public void CalculateDistance_OnTheSamePieceButEndBeforeStart_CorrectResult()
        {
            var startTrackPiece = TrackPiece.CreateStraightPiece(100);
            var middleTrackPiece = TrackPiece.CreateStraightPiece(100);
            var finishTrackPiece = TrackPiece.CreateStraightPiece(200);
            var startPosition = new Position() { TrackPiece = middleTrackPiece, PieceDistance = 50 };
            var finishPosition = new Position() { TrackPiece = middleTrackPiece, PieceDistance = 40 };

            RaceTrack track = new RaceTrack();
            var node = track._raceTrack.AddFirst(startTrackPiece);
            node = track._raceTrack.AddAfter(node, middleTrackPiece);
            track._raceTrack.AddAfter(node, finishTrackPiece);

            Assert.Equal(390, track.CalculateDistance(startPosition, finishPosition));
        }
    }
}